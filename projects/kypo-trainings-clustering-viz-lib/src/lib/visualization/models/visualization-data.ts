import { ClusterVisualizationData } from './cluster-visualization-data';
import { EuclidianDoublePoint } from './eucledian-double-point';

export class VisualizationData {
  radarData?: EuclidianDoublePoint[];
  clusterData?: ClusterVisualizationData[];
}
